# Steps to implement

1. Two numbers arranged left and right
1. Procedure end when left one reaches "1"
1. Left column value is halved, right column value is doubled
1. Results are turned into integers by always rounding down in left column (truncating)
1. If an even number is in left column, corresponding right one is eliminated from procedure
1. All numbers in the right column are added (except where left is even) and give the result

