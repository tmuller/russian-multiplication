class RussianMultiplication {

  multiply(left, right) {
    const valuePairs = this.iterate(left, right);
    return valuePairs.reduce((acc, curr) => {
      return acc + curr[1];
    }, 0);
  }

  iterate(left, right) {
    let steps = [];
    while (left > 0) {
      if (left % 2 !== 0) steps.push([left, right]);
      left = Math.trunc(left / 2);
      right = right * 2;
    }

    return steps;
  }
}

module.exports = RussianMultiplication;
