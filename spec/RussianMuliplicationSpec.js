const RussianMultiplication = require('../src/RussianMultiplication');

describe('RussianMultiplication', () => {
  describe('multiply', () => {

    let instance;

    beforeEach(() => {
      instance = new RussianMultiplication();
    })

    it('should return the result when the left column contains the value "1"', () => {
      const result = instance.multiply(1, 2);
      expect(result).toEqual(2);
    });

    it('should double right and halve left value', () => {
      const result = instance.multiply(3, 4);
      expect(result).toEqual(12);
    });

    it('should not add number if left one is even', () => {
      const result = instance.multiply(2, 4);
      expect(result).toEqual(8);
    });

    it('should not give different result if operands are switched', () => {
      const instance2 = new RussianMultiplication();
      const result1 = instance.multiply(2, 4);
      const result2 = instance2.multiply(4, 2);
      expect(result1).toEqual(8);
      expect(result2).toEqual(8);
    });

    it('should correctly solve the problem in the blog post', () => {
      const result = instance.multiply(47, 42)
      expect(result).toEqual(1974);
    });

  });
});
